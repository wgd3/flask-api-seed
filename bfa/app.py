from flask import Flask

from bfa.extensions import db, cors, migrate, guard
from bfa.database.models import User
from bfa.apis import v1


def create_app(config="bfa.settings"):
    app = Flask(__name__.split(".")[0])
    app.config.from_object(config)

    register_extensions(app)
    register_blueprints(app)

    return app


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app)
    guard.init_app(app, user_class=User)

    return None


def register_blueprints(app):
    app.register_blueprint(v1.bp)

    return None
