from bfa.database.models import User
from bfa.extensions import db, guard
from .base_core import BaseCore


class UserCore(BaseCore):
    def get_all_users(self, page=1, per_page=10, **kwargs):
        return self.get_all_resources(
            query=User.query, endpoint="api_v1.users", page=page, per_page=per_page
        )

    def get_user(self, user_id, **kwargs):
        return self.get_resource(User, user_id)

    def update_user(self, user_id, data):
        return self.update_resource(User, user_id, data)

    def delete_user(self, user_id):
        return self.delete_resource(User, user_id)

    @staticmethod
    def create_user(
        username: str, password: str, email: str = None, **kwargs
    ) -> object:
        """
        Method for handling user creation. Returns the new user details as well as access and refresh tokens

        Args:
            username:
            password:
            email:
        """
        try:
            new_user = User.create(
                username=username, password_hash=password, email=email, **kwargs
            )
            ret = dict(
                message=f"User {username} created successfully!",
                data=new_user.to_json(),
            )
            token = guard.encode_jwt_token(new_user)
            ret["data"]["access_token"] = token
            ret["data"]["refresh_token"] = guard.refresh_jwt_token(token)
            return ret
        except Exception as e:
            print(e)
            return None
