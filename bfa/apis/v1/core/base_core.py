from flask import current_app as app, url_for

import logging

from sqlalchemy.exc import OperationalError, IntegrityError

logger = logging.getLogger("mtm")


class BaseCore(object):
    @staticmethod
    def create_resource(resource, payload):
        app.logger.debug("Creating new database record for {}".format(str(resource)))
        try:
            item = resource.create(**payload)
            return BaseCore._generate_response(
                "Resource created successfully!", item.to_json()
            )
        except IntegrityError as ie:
            # raise MtmDuplicateEntryError(
            #     message="This record was unable to be added to the database! One or more foreign keys are invalid",
            #     data=ie.detail if ie.detail else None,
            # )
            pass

    @staticmethod
    def get_resource(resource, resource_id, additional_fields=None):
        if additional_fields is None:
            additional_fields = []
        else:
            app.logger.debug(
                "additional_fields: {}".format(", ".join(additional_fields))
            )
        app.logger.debug(
            "Attemping to retrieve resource {} with ID #{}".format(
                str(resource), resource_id
            )
        )
        if len(additional_fields) > 0:
            app.logger.debug(
                "Retrieving resource with additional fields: {}".format(
                    ", ".join(additional_fields)
                )
            )

        item = resource.query.get_or_404(resource_id)
        return BaseCore._generate_response(
            "Resource retrieved", item.to_dict(show=additional_fields)
        )

    @staticmethod
    def update_resource(resource, resource_id, payload):
        app.logger.debug(
            "Attempting to update resource with the following fields:\n{}".format(
                payload
            )
        )
        item = resource.query.get_or_404(resource_id)
        try:
            item.update(**payload)
            return BaseCore._generate_response(
                "Resource updated successfully", item.to_dict()
            )
        except IntegrityError as ie:
            # raise MtmDuplicateEntryError(
            #     message="Measurement model requires a unique name",
            #     data=ie.detail if ie.detail else None,
            # )
            pass
        except OperationalError as oe:
            # raise MtmDatabaseOperationsError(
            #     message="Database error occurred while updating this model",
            #     data=oe.detail if oe.detail else None,
            # )
            pass
        except Exception as e:
            # raise MtmUnknownError(message="Unknown error occurred", data=e)
            pass

    @staticmethod
    def delete_resource(resource, resource_id):
        app.logger.debug("Deleting resource with ID #{}".format(resource_id))
        item = resource.query.get_or_404(resource_id)
        try:
            item.delete()
            return BaseCore._generate_response("Resource deleted successfully")
        except OperationalError as oe:
            # raise MtmDatabaseOperationsError(
            #     message="Database error occurred while updating this model",
            #     data=oe.detail if oe.detail else None,
            # )
            pass
        except Exception as e:
            # raise MtmUnknownError(message="Unknown error occurred", data=e)
            pass

    def get_all_resources(
        self,
        query,
        endpoint,
        page=1,
        per_page=10,
        error_out=True,
        max_per_page=20,
        **kwargs
    ):
        """
        This function returns a paginated list response for the API.
        :param query: SQLAlchemy Query
        :param endpoint:
        :param page:
        :param per_page:
        :param error_out:
        :param max_per_page:
        :param kwargs: should only be used for passing url parameters for a url_for function
        :return:
        """
        app.logger.debug("Getting paginated list...")
        try:
            pagination = query.paginate(
                page, per_page, error_out=error_out, max_per_page=max_per_page
            )
            items = [i.to_json() for i in pagination.items]
            meta = self._generate_meta(pagination)
            links = self._generate_links(pagination, endpoint, **kwargs)
            return self._generate_response(
                "{} items found".format(len(items)), items, meta, links
            )

        except OperationalError as oe:
            # raise MtmDatabaseOperationsError(
            #     message="Database error occurred while retrieving measurement models!",
            #     data=oe.detail if oe.detail else None,
            # )
            app.logger.error(oe)

        except Exception as e:
            # raise MtmUnknownError(message="Unknown error occurred", data=e)
            app.logger.error(e)

    @staticmethod
    def _generate_meta(pagination_object):
        """
        Creates the 'meta' dictionary used for reporting pagination state
        Args:
            pagination_object (flask_sqlalchemy.Pagination):

        Returns:
            meta_dict (dict):
        """
        return dict(
            total_pages=pagination_object.pages,
            page=pagination_object.page,
            per_page=pagination_object.per_page,
            total_items=pagination_object.total,
        )

    @staticmethod
    def _generate_links(pagination_object, endpoint, **kwargs):
        return dict(
            next=url_for(endpoint, page=pagination_object.next_num, **kwargs)
            if pagination_object.has_next
            else None,
            prev=url_for(endpoint, page=pagination_object.prev_num, **kwargs)
            if pagination_object.has_prev
            else None,
        )

    @staticmethod
    def _generate_response(message, data=None, meta=None, links=None):
        return dict(message=message, data=data, meta=meta, links=links)
