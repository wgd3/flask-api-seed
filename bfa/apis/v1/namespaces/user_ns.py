from http import HTTPStatus

from flask_restplus import Namespace, Resource, fields

from ..response_model import resp_single_envelope, resp_list_envelope
from ..core import UserCore

ns = Namespace("users", title="User Endpoints", validate=True)

single_envelope = ns.model(
    resp_single_envelope.get("name"), resp_single_envelope.get("model")
)
list_envelope = ns.model(
    resp_list_envelope.get("name"), resp_list_envelope.get("model")
)
new_user_envelope = ns.model(
    "New User Payload",
    {
        "username": fields.String(required=True, description="Username"),
        "password": fields.String(required=True, description="Secure Password"),
        "email": fields.String(required=False, description="(optional)"),
    },
)


@ns.route("", endpoint="users")
@ns.response(
    HTTPStatus.INTERNAL_SERVER_ERROR.value, HTTPStatus.INTERNAL_SERVER_ERROR.name
)
class UserListResource(Resource):
    @ns.response(HTTPStatus.OK.value, HTTPStatus.OK.name)
    def get(self):
        return UserCore().get_all_users()

    @ns.response(HTTPStatus.CREATED.value, HTTPStatus.CREATED.name)
    @ns.response(HTTPStatus.BAD_REQUEST.value, HTTPStatus.BAD_REQUEST.name)
    @ns.expect(new_user_envelope)
    def post(self):
        return UserCore().create_user(**ns.payload)


@ns.route("/<int:user_id>", endpoint="user")
@ns.response(
    HTTPStatus.INTERNAL_SERVER_ERROR.value, HTTPStatus.INTERNAL_SERVER_ERROR.name
)
@ns.response(HTTPStatus.NOT_FOUND.value, HTTPStatus.NOT_FOUND.name)
class UserResource(Resource):
    @ns.response(HTTPStatus.OK.value, HTTPStatus.OK.name)
    def get(self, user_id):
        pass

    @ns.response(HTTPStatus.BAD_REQUEST.value, HTTPStatus.BAD_REQUEST.name)
    def put(self, user_id):
        pass

    def delete(self, user_id):
        pass
