from http import HTTPStatus

from flask_restplus import Namespace, Resource

from ..response_model import resp_single_envelope
from ..core import AuthCore

ns = Namespace("auth", title="Authentication Endpoints")

single_envelope = ns.model(
    resp_single_envelope.get("name"), resp_single_envelope.get("model")
)


@ns.route("/login")
@ns.response(
    HTTPStatus.INTERNAL_SERVER_ERROR.value, HTTPStatus.INTERNAL_SERVER_ERROR.name
)
@ns.response(HTTPStatus.OK.value, HTTPStatus.OK.name)
@ns.response(HTTPStatus.BAD_REQUEST.value, HTTPStatus.BAD_REQUEST.name)
class LoginResource(Resource):
    def post(self):
        pass
