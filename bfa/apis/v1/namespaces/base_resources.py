from abc import ABC, abstractmethod


class BaseListResource(ABC):
    """Base class for resource endpoints which return a list or accepts a POST"""

    @abstractmethod
    def get(self):
        pass

    @abstractmethod
    def post(self):
        pass


class BaseSingleResource(ABC):
    """Base class for resource endpoints which refer to a specific resource"""

    @abstractmethod
    def get(self, resource_id: int):
        pass

    @abstractmethod
    def put(self, resource_id: int):
        pass

    @abstractmethod
    def delete(self, resource_id: int):
        pass
