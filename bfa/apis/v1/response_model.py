from flask_restplus import fields

resp_single_envelope = {
    "name": "Response payload",
    "model": {"message": fields.String, "data": fields.Raw},
}

resp_list_envelope = {
    "name": "Response payload",
    "model": {"message": fields.String, "data": fields.List(fields.Raw)},
}
