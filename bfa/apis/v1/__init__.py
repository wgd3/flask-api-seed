import flask_buzz
from flask import Blueprint
from flask_restplus import Api

from .namespaces import user_ns, auth_ns

bp = Blueprint("api_v1", __name__, url_prefix="/api/v1")
api = Api(bp, version="1.0", doc="/docs")

# necessary to make sure flask-restplus' error handler wires up correctly
flask_buzz.FlaskBuzz.register_error_handler_with_flask_restplus(api)

api.add_namespace(user_ns, path="/users")
api.add_namespace(auth_ns, path="/auth")
