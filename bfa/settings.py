from environs import Env

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="development")
DEBUG = ENV == "development"
SQLALCHEMY_DATABASE_URI = env.str("DATABASE_URL", default="sqlite:///dev.db")
SECRET_KEY = env.str("SECRET_KEY", default="CHANGEME")
CACHE_TYPE = "simple"  # Can be "memcached", "redis", etc.
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Flask-Praetorian settings
JWT_ACCESS_LIFESPAN = {"minutes": 15}
JWT_REFRESH_LIFESPAN = {"days": 30}
