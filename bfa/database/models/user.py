from bfa.extensions import db, guard
from bfa.database import (
    Model,
    TimestampMixin,
    JsonSerializer,
    PaginatedAPIMixin,
    reference_col,
    relationship,
)
from bfa.database.utils import SurrogatePK


class UserJSONSerializer(JsonSerializer):
    __json_public__ = ["id", "username", "email", "active"]
    __json_hidden__ = ["_password_hash"]


class User(Model, SurrogatePK, TimestampMixin, UserJSONSerializer):

    __tablename__ = "users"

    # id - provided by Model base class)
    # created - provided by TimestampMixin
    # updated - provided by TimestampMixin
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(255), unique=True)
    _password_hash = db.Column(db.String(120))
    active = db.Column(db.Boolean(), default=False)
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(100))
    current_login_ip = db.Column(db.String(100))
    login_count = db.Column(db.Integer)
    registered_at = db.Column(db.DateTime())

    def _get_password_hash(self):
        return self._password_hash

    def _set_password_hash(self, password):
        self._password_hash = guard.hash_password(password)

    # Hide password encryption by exposing password field only.
    password_hash = db.synonym(
        "_password_hash", descriptor=property(_get_password_hash, _set_password_hash)
    )

    def check_password(self, password):
        return guard.authenticate(self.username, password)

    @classmethod
    def lookup(cls, username):
        """
        Method required by Flask-Praetorian to retrieve users

        Args:
            username:
        """
        return cls.query.filter(cls.username == username).one_or_none()

    @classmethod
    def identify(cls, user_id):
        """
        Method required by Flask-Praetorian

        Args:
            user_id:
        """
        return cls.get_by_id(user_id)

    @property
    def rolenames(self):
        """
        Method required by Flask-Praetorian (currently unused)

        Returns:

        """
        return []

    @property
    def password(self):
        """
        Flask-Praetorian required a getter for a hashed password
        Returns:

        """
        return self._password_hash

    @property
    def identity(self) -> int:
        """
        Required by Flask-Praetorian for user lookup
        """
        return self.id

    def is_valid(self) -> bool:
        """
        Required by Flask-Praetorian
        Returns:

        """
        return self.active
